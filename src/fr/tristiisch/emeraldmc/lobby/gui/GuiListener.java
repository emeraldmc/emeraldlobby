package fr.tristiisch.emeraldmc.lobby.gui;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class GuiListener implements Listener {

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}
		if(clickedInventory.getType().equals(InventoryType.PLAYER)) {
			if(player.getGameMode().equals(GameMode.CREATIVE)) {
				return;
			}
			event.setCancelled(true);
			player.updateInventory();
		}
		final ItemStack currentItem = event.getCurrentItem();

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "menu":
				event.setCancelled(true);
				if(GuiUtils.isSame(currentItem, GuiUtils.Skywars)) {
					GuiUtils.gotoServer(player, "skywars1", event);
					return;

				} else if(GuiUtils.isSame(currentItem, GuiUtils.Skyfight)) {
					GuiUtils.gotoServer(player, "skyfight1", event);
					return;

				} else if(GuiUtils.isSame(currentItem, GuiUtils.Braintournement)) {
					GuiUtils.gotoServer(player, "braintournement1", event);
					return;
				} else if(GuiUtils.isSame(currentItem, GuiUtils.Buildeur)) {
					GuiUtils.gotoServer(player, "buildeur", event);
					return;
				}
				break;
			case "skywars":
				event.setCancelled(true);
				break;
			case "braintournement":
				event.setCancelled(true);
				break;

			case "profil":
				event.setCancelled(true);
				if(event.getClickedInventory() != null && event.getInventory().getName().equals(event.getClickedInventory().getName()) && !currentItem.isSimilar(GuiUtils.Cancel) && !event
						.getCurrentItem()
						.isSimilar(ItemTools.create(Material.AIR, 0, (short) -1))) {
					GuiUtils.cancelItem(event, "En développement");
					return;
				}
				break;
			}
		}

		GuiUtils.hotbar(player, currentItem);
	}

	@EventHandler
	private void PlayerInteractEvent(final PlayerInteractEvent event) {
		GuiUtils.hotbar(event.getPlayer(), event.getItem());
	}

}

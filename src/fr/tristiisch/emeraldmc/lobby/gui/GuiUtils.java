package fr.tristiisch.emeraldmc.lobby.gui;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.Boutique;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn.Spawn;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotBPMC;
import fr.tristiisch.emeraldmc.lobby.Main;

public class GuiUtils {

	/*HashMap<String, ItemStack> items = new HashMap<>();*/

	public static ItemStack Braintournement = ItemTools
			.create(Material.SANDSTONE, 1, "&2BrainTournement", "", "&7%online% connecté%online_n% sur %max%", "", "&6En cours de développement", "", "&7Statut: %status%", "");
	public static ItemStack Buildeur = ItemTools
			.create(Material.STONE, 1, "&2Serveur Buildeur", "", "&7%online% connecté%online_n% sur %max%", "", "&bRéservé aux buildeurs", "", "&7Statut: %status%", "");
	public static ItemStack Cancel = ItemTools.create(Material.REDSTONE_BLOCK, 1, "&4✖ &lImpossible");
	public static ItemStack compass = ItemTools.create(Material.COMPASS, 1, "&6Menu");

	public static ItemStack shop = ItemTools.create(Material.ENDER_CHEST, 1, "&7Boutique");
	public static ItemStack skull = ItemTools.createPlayerSkull(null, 1, "&7Profil");
	public static ItemStack Skyfight = ItemTools.create(Material.EMERALD_BLOCK, 1, "&2SkyFight", "", "&7%online% connecté%online_n% sur %max%", "", "&bMélée générale", "", "&7Statut: %status%", "");
	public static ItemStack Skywars = ItemTools.create(Material.CHEST, 1, "&2SkyWars", "", "&7%online% connecté%online_n% sur %max%", "", "&6En cours de développement", "", "&7Statut: %status%", "");

	public static ItemStack spawn = ItemTools.create(Material.BED, 1, "&7Retour au spawn");

	public static void cancelItem(final InventoryClickEvent event, final String msg) {
		event.setCancelled(true);
		final Player player = (Player) event.getWhoClicked();
		final ItemStack item = event.getCurrentItem();
		event.getClickedInventory().setItem(event.getSlot(), ItemTools.setLore(GuiUtils.Cancel, Arrays.asList("", "&c" + msg, "")));
		player.playSound(player.getLocation(), Sound.VILLAGER_NO, 1, 1);
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> event.getClickedInventory().setItem(event.getSlot(), item), 30);
	}

	public static void gotoServer(final Player player, final String serverName, final InventoryClickEvent event) {
		final EmeraldServer emeraldServer = EmeraldServers.getServer(serverName);
		if(emeraldServer == null) {
			cancelItem(event, "Merci de réessayer plus tard");
			return;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(emeraldServer.getStatus().isOpen() || new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerMoreThan(EmeraldGroup.RESPBUILD)) {
				player.closeInventory();
				SpigotBPMC.connect(player, serverName);
			} else {
				cancelItem(event, "Le serveur est " + emeraldServer.getStatus().getNameColored());
			}
		});
	}

	public static void hotbar(final Player player, final ItemStack item) {
		if(item != null && item.getItemMeta() != null && item.getItemMeta().hasDisplayName()) {

			final String itemname = item.getItemMeta().getDisplayName();

			if(itemname.equals(GuiUtils.compass.getItemMeta().getDisplayName())) {

				GuiUtils.openMenu(player);

			} else if(itemname.equals(GuiUtils.shop.getItemMeta().getDisplayName())) {

				player.closeInventory();
				Boutique.openMenu(player);

			} else if(itemname.equals(GuiUtils.skull.getItemMeta().getDisplayName())) {

				Profil.openProfil(player);

			} else if(itemname.equals(GuiUtils.spawn.getItemMeta().getDisplayName())) {

				player.closeInventory();
				Spawn.telportToSpawn(player, Spawn.getDefaultTime());
			}
		}
	}

	public static boolean isSame(final ItemStack a, final ItemStack b) {
		return a.getType() == b.getType() && a.getItemMeta().getDisplayName() == b.getItemMeta().getDisplayName();
	}

	public static void openBraintournement(final Player player) {
		final GuiTools inventory = new GuiTools("&2BrainTournement", "braintournement", 1);
		inventory.openInventory(player);
	}

	public static void openMenu(final Player player) {
		final GuiTools inventory = new GuiTools("&2Menu", "menu", 9 * 3);
		inventory.setItem(inventory.getMiddleSlot() - 2, serverInfoToItem(EmeraldServers.getServer("skywars1"), Skywars));

		inventory.setItem(inventory.getMiddleSlot(), serverInfoToItem(EmeraldServers.getServer("skyfight1"), Skyfight));

		inventory.setItem(inventory.getMiddleSlot() + 2, serverInfoToItem(EmeraldServers.getServer("braintournement1"), Braintournement));

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(new AccountProvider(player.getUniqueId()).getEmeraldPlayer().getGroup().isStaffMember()) {
				inventory.setItem(inventory.getLastSlot(), serverInfoToItem(EmeraldServers.getServer("buildeur"), Buildeur));
			}
		});
		inventory.openInventory(player);
	}

	public static void openSkywars(final Player player) {
		final GuiTools inventory = new GuiTools("&2SkyWars", "skywars", 1);
		inventory.openInventory(player);
	}

	private static ItemStack serverInfoToItem(final EmeraldServer server, final ItemStack item) {

		if(server == null) {
			return ItemTools.setLore(item.clone(), Arrays.asList("", "&cAucune informations", ""));
		}
		final HashMap<String, String> replace = new HashMap<>();
		if(server.getInfo() != null) {
			replace.put("%online%", String.valueOf(server.getInfo().getPlayers().getOnline()));
			replace.put("%online_n%", Utils.withOrWithoutS(server.getInfo().getPlayers().getOnline()));
			replace.put("%max%", String.valueOf(server.getInfo().getPlayers().getMax()));
		} else {
			replace.put("%online%", "0");
			replace.put("%online_n%", "");
			replace.put("%max%", "0");
		}
		replace.put("%status%", server.getStatus().getColor() + StringUtils.capitalize(server.getStatus().getName()));

		return ItemTools.replaceInLore(item.clone(), replace);
	}

	public static void setHotbar(final Player player) {
		player.getInventory().setItem(0, ItemTools.setOwner(skull, player.getName()));
		player.getInventory().setItem(1, shop);
		player.getInventory().setItem(8, spawn);
		player.getInventory().setItem(4, compass);
	}
}

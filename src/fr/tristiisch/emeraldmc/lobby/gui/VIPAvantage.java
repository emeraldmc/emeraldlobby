package fr.tristiisch.emeraldmc.lobby.gui;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemGlow;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import net.md_5.bungee.api.ChatColor;

public enum VIPAvantage {

	EMERAUDE(EmeraldGroup.EMERAUDE, ChatColor.GOLD, 46000, 14000, ItemTools
			.create(Material.EMERALD_BLOCK, 1, "&6Emeraude", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique"), ItemGlow.getItemGlowEnchant())),
	DIAMANT(EmeraldGroup.DIAMANT, ChatColor.AQUA, 36000, 11000, ItemTools
			.create(Material.DIAMOND_BLOCK, 1, "&bDiamant", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique"))),
	SAPHIR(EmeraldGroup.SAPHIR, ChatColor.DARK_AQUA, 26000, 8000,
			ItemTools.create(Material.LAPIS_BLOCK, 1, "&3Saphir", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique"))),
	RUBIS(EmeraldGroup.RUBIS, ChatColor.YELLOW, 16000, 5000,
			ItemTools.create(Material.NETHERRACK, 1, "&eRubis", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique")));

	final private EmeraldGroup group;
	final private ChatColor color;
	final private int priceLife;
	final private int priceMonth;
	final private ItemStack item;

	private VIPAvantage(final EmeraldGroup group, final ChatColor color, final int priceLife, final int priceMonth, final ItemStack item) {
		this.group = group;
		this.color = color;
		this.priceLife = priceLife;
		this.priceMonth = priceMonth;
		this.item = item;
	}

	public String getName() {
		return this.group.getName();
	}

	public String getNameColored() {
		return this.color + this.group.getName();
	}

	public EmeraldGroup getGroup() {
		return this.group;
	}

	public ChatColor getColor() {
		return this.color;
	}

	public int getPriceLife() {
		return this.priceLife;
	}

	public int getPriceMonth() {
		return this.priceMonth;
	}

	public ItemStack getItem() {
		return this.item;
	}

	public static VIPAvantage get(final ItemStack item) {
		return Arrays.stream(VIPAvantage.values()).filter(vip -> item.isSimilar(vip.getItem())).findFirst().orElse(null);
	}

	public static VIPAvantage get(final String name) {
		return Arrays.stream(VIPAvantage.values()).filter(vip -> vip.toString().equalsIgnoreCase(name)).findFirst().orElse(null);
	}
}

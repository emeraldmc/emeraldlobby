package fr.tristiisch.emeraldmc.lobby.gui;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class Profil {

	public static ItemStack skullInfo = ItemTools.createPlayerSkull(null, 1, "&7Info");

	public static void openProfil(final Player player) {
		final GuiTools inventory = new GuiTools("&2Profile", "profil", 5);
		int i = inventory.getFirstSlot();
		inventory.setItem(i++, ItemTools.setOwner(skullInfo, player.getName()));
		inventory.setItem(i++, ItemTools.create(Material.NETHER_STAR, 1, (short) 0, Utils.color("&7Vos succès"), Arrays.asList("", Utils.color("&6En cours de développement !"), "")));
		inventory.setItem(i++, ItemTools.create(Material.STICK, 1, (short) 0, Utils.color("&7Vos bans"), Arrays.asList("", Utils.color("&6En cours de développement !"), "")));
		inventory.setItem(i++, ItemTools.createPlayerSkull(null, 1, Utils.color("&7Vos Amis"), Arrays.asList("", Utils.color("&6En cours de développement !"), "")));
		inventory.setItem(i++, ItemTools.create(Material.MINECART, 1, (short) 0, Utils.color("&7Vos stats"), Arrays.asList("", Utils.color("&6En cours de développement !"), "")));

		inventory.openInventory(player);
	}
}

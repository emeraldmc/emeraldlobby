package fr.tristiisch.emeraldmc.lobby;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn.Spawn;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.CancelFoodListener;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.TchatFormat;
import fr.tristiisch.emeraldmc.lobby.gui.GuiListener;
import fr.tristiisch.emeraldmc.lobby.listeners.DamageListener;
import fr.tristiisch.emeraldmc.lobby.listeners.JoinListener;
import fr.tristiisch.emeraldmc.lobby.listeners.ProtectListener;
import fr.tristiisch.emeraldmc.lobby.scoreboard.ScoreboardListener;

public class Main extends JavaPlugin {

	public static Main instance;

	public static Main getInstance() {
		return instance;
	}

	@Override
	public void onDisable() {
		Bukkit.getServer().getScoreboardManager().getMainScoreboard().getTeams().forEach(t -> t.unregister());
		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&c" + this.getDescription().getName() + " (V" + this.getDescription().getVersion() + ") &cest désactivé."));

	}

	@Override
	public void onEnable() {
		instance = this;

		final PluginManager pluginmanager = this.getServer().getPluginManager();

		// Gestion Commands

		// Gestion Events
		pluginmanager.registerEvents(new JoinListener(), this);
		pluginmanager.registerEvents(new TchatFormat(), this);
		pluginmanager.registerEvents(new GuiListener(), this);
		pluginmanager.registerEvents(new CancelFoodListener(), this);
		pluginmanager.registerEvents(new ScoreboardListener(), this);
		pluginmanager.registerEvents(new DamageListener(), this);
		pluginmanager.registerEvents(new ProtectListener(), this);

		Spawn.setDefaultTime(0);
		EmeraldSpigot.setSpawn(new Location(Bukkit.getWorlds().get(0), 0.5, 8.2, 0.5, -180, 0));
		this.getServer().setDefaultGameMode(GameMode.ADVENTURE);

		for(final Player player : this.getServer().getOnlinePlayers()) {
			JoinListener.init(player);
		}

		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&a" + this.getDescription().getName() + " (V" + this.getDescription().getVersion() + ") est activé."));
	}
}

package fr.tristiisch.emeraldmc.lobby.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.Team;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.clear.Clear;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.api.spigot.title.Title;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.lobby.gui.GuiUtils;
import fr.tristiisch.emeraldmc.lobby.scoreboard.Scoreboards;

public class JoinListener implements Listener {

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		event.setJoinMessage(null);
		final Player player = event.getPlayer();
		Title.sendTitle(player, "&4[&cBETA&4] &2EmeraldMC", "&6SkyFight - SkyWars - BrainTournement", 20, 40, 20);
		init(player);
		GuiUtils.setHotbar(player);
	}

	@EventHandler
	public void AsyncEmeraldPlayerLoad(final AsyncEmeraldPlayerLoadEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();

		Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
			Scoreboards.setScoreboards(player, emeraldPlayer);
		});
		if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.RUBIS)) {
			event.setJoinMessageFormat("&7[&a+&7] %prefix%%name%");
		}
		if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.ADMIN)) {
			if(!player.isOp()) {
				player.setOp(true);
			}
		} else {
			if(player.isOp()) {
				player.setOp(false);
			}
		}
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		event.setQuitMessage(null);
		final Player player = event.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {

			final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.RUBIS)) {
				Bukkit.broadcastMessage(
						Utils.color(EmeraldSpigot.getInstance().getConfig().getString("spigot.settings.quitmsg").replaceAll("%prefix%", emeraldPlayer.getGroup().getPrefix()).replaceAll("%name%",
								player.getName())));
			}
		});

		for(final Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			team.removeEntry(player.getName());
		}
		init(player);
		Scoreboards.removeScoreboard(player);
	}

	public static void init(final Player player) {
		Clear.player(player);
		player.setGameMode(GameMode.ADVENTURE);
		for(final PotionEffect effect : player.getActivePotionEffects()) {
			player.removePotionEffect(effect.getType());
		}
		player.setMaxHealth(2);
		player.setHealth(player.getMaxHealth());
		player.setWalkSpeed(0.2f);
		player.setAllowFlight(false);
		player.setFlying(false);
		player.setFlySpeed(0.5f);
		player.setFoodLevel(20);
		player.setExp(0);
		player.setFlying(false);
		player.setCanPickupItems(false);
		SpigotUtils.addNightVision(player);
		player.teleport(EmeraldSpigot.getSpawn());
	}

	/*
	 * @EventHandler public void EmeraldPlayerLoad(final EmeraldPlayerLoad event) {
	 * final Player player = event.getPlayer(); final EmeraldPlayer emeraldPlayer =
	 * event.getEmeraldPlayer(); GuiUtils.setHotbar(player);
	 * Scoreboards.setScoreboards(player, emeraldPlayer); }
	 */
}
